<?php
/**
 * PHP Version 7.2
 *
 * @category Controllers
 * @package  App\Http\Controllers
 * @author   Thiago Mallon <thiagomallon@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/thiago-mallon/
 */

/**
 * File namespace
 *
 * @subpackage Http\Controllers
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HomeMessageRequest;
use App\Events\PublicMessageSent;

/**
 * Class DashboardController
 *
 * @category Controllers
 * @package  App\Http\Controllers
 * @author   Thiago Mallon <thiagomallon@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/thiago-mallon/
 */
class DashboardController extends Controller
{
    /**
     * Public method __construct - The constructor method
     *
     * @method void __construct The constructor method
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Public method index - The index action
     *
     * @method object index The index action
     * @return object Returns a view object
     */
    public function index(): object
    {
        return view('dashboard.home');
    }

    /**
     * Public method sendPublicMessage - Method dispatch the broadcasting event
     *
     * @param \App\Http\Requests\HomeMessageRequest $request The request data
     *
     * @method string sendPublicMessage Method dispatch the broadcasting event
     * @return string
     */
    public function sendPublicMessage(HomeMessageRequest $request)
    {
        $number = $request->message;
        $count = 50;
        try {
            for ($i = 0; $i <= $count; $i++) {
                $sixDigitRandomNumber = rand(100000, 999999);
                auth()->user()->homeMessages()
                    ->create(['message' => $sixDigitRandomNumber]);
               // $this->displayMessage($sixDigitRandomNumber);
                //sleep for 1 Second
                echo $sixDigitRandomNumber;
                usleep(1000000);

            }

        } catch (\Illuminate\Database\QueryException $ex) {

        }
    }
    public function displayMessage($message){
        PublicMessageSent::dispatch($message);
        return response(request('message'), 200);
    }
}
